package com.example.exament3.Services;

import com.example.exament3.Entities.Pokemon;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface PokemonService {
    @GET("pokemons/15404")
    Call<List<Pokemon>> getPokemon();

    @POST("pokemons/15404/crear")
    Call<Void> createPokemon(@Body Pokemon pokemon);

    @GET("pokemons/{id}/mostrar")
    Call<Pokemon> getPokemonDetail(@Path("id") String id);
}
