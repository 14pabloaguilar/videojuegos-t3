package com.example.exament3.Entities;

public class Pokemon {
    public String id;
    public String nombre;
    public String tipo;
    public String url_imagen;
    public int esta_atrapado;

    public Pokemon(String nombre, String tipo, String url_imagen) {
        this.nombre = nombre;
        this.tipo = tipo;
        this.url_imagen = url_imagen;
    }

}
