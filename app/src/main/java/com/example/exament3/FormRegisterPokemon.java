package com.example.exament3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.exament3.Entities.Pokemon;
import com.example.exament3.Services.PokemonService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FormRegisterPokemon extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_register_pokemon);

        EditText edtNombre = findViewById(R.id.edtNameP);
        EditText edtTipo = findViewById(R.id.edtTypeP);
        EditText edtImagen = findViewById(R.id.edtImageP);
        EditText edtLatitud = findViewById(R.id.edtLatitudeP);
        EditText edtLongitud = findViewById(R.id.edtLongitudeP);
        Button btnSubmit = findViewById(R.id.btnSubmitPokemon);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://upn.lumenes.tk/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        PokemonService service = retrofit.create(PokemonService.class);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String nombre = edtNombre.getText().toString();
                String tipo = edtTipo.getText().toString();
                String imagen = edtImagen.getText().toString();


                Pokemon pokemon = new Pokemon(nombre, tipo, imagen);

                Log.i("MAIN_APP", "HE AQUI EL NOMBREEEE: " + nombre);

                if (!nombre.trim().equals("") && !tipo.trim().equals("") && !imagen.trim().equals("")) //si texto es diferente de vacio
                {
                    Call<Void> tareaCall = service.createPokemon(pokemon);
                    tareaCall.enqueue(new Callback<Void>() {
                        @Override
                        public void onResponse(Call<Void> call, Response<Void> response) {
                            //codigo que recibe al haber esperado el call

                        }

                        @Override
                        public void onFailure(Call<Void> call, Throwable t) {

                        }
                    });
                    Intent intent = new Intent(FormRegisterPokemon.this, MainActivity.class);
                    startActivity(intent);
                } else {
                    Log.i("main_app", "Ingrese nombre");
                    Toast.makeText(FormRegisterPokemon.this, "Inserte los datos!", Toast.LENGTH_SHORT).show();//muestra mensaje en pantalla
                }

            }
        });


    }
}