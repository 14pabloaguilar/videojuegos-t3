package com.example.exament3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FloatingActionButton btnView = findViewById(R.id.btnViewPokemons);

        btnView.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, ViewPokemos.class);
            startActivity(intent);
        });

        FloatingActionButton btnAdd = findViewById(R.id.btnAddPokemons);

        btnAdd.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, FormRegisterPokemon.class);
            startActivity(intent);
        });
    }
}