package com.example.exament3.Adapters;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.exament3.DetailPokemon;
import com.example.exament3.Entities.Pokemon;
import com.example.exament3.R;
import com.example.exament3.Services.PokemonService;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PokemonAdapter extends RecyclerView.Adapter<PokemonAdapter.PokemonViewHolder> {
    List<Pokemon> pokemons;

    public PokemonAdapter(List<Pokemon> pokemons) {
        this.pokemons = pokemons;
    }

    @Override
    public PokemonViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pokemon,parent, false);
        return new PokemonViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PokemonViewHolder holder, int position) {
        View view = holder.itemView;
        Pokemon pokemon = pokemons.get(position);

        TextView tvName = view.findViewById(R.id.tvNamePokemon);
        TextView tvType = view.findViewById(R.id.tvTypePokemon);
        ImageView tvImagen = view.findViewById(R.id.imgPokemonList);

        tvName.setText(pokemon.nombre);
        tvType.setText(pokemon.tipo);
        Picasso.get().load(pokemon.url_imagen).error(R.drawable.img_pokemon).into(tvImagen); //PICASSO

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl("https://upn.lumenes.tk/")
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                PokemonService service = retrofit.create(PokemonService.class);
                Call<Pokemon> tareaDetail = service.getPokemonDetail(pokemon.id);

                tareaDetail.enqueue(new Callback<Pokemon>() {
                    @Override
                    public void onResponse(Call<Pokemon> call, Response<Pokemon> response) {

                        Intent intent = new Intent(view.getContext(), DetailPokemon.class);
                        intent.putExtra("id", pokemon.id);
                        intent.putExtra("nombre", pokemon.nombre);
                        intent.putExtra("tipo", pokemon.tipo);
                        intent.putExtra("url_imagen", pokemon.url_imagen);
                        intent.putExtra("esta_atrapado", pokemon.esta_atrapado);
                        view.getContext().startActivity(intent);

                    }

                    @Override
                    public void onFailure(Call<Pokemon> call, Throwable t) {

                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return pokemons.size();
    }


    public static class PokemonViewHolder extends RecyclerView.ViewHolder {
        public PokemonViewHolder(View itemView) {
            super(itemView);
        }
    }
}
