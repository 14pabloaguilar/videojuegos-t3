package com.example.exament3;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class DetailPokemon extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_pokemon);

        String id = getIntent().getStringExtra("id");
        String nombre = getIntent().getStringExtra("nombre");
        String tipo = getIntent().getStringExtra("tipo");
        String imagen = getIntent().getStringExtra("url_imagen");
        String ea = getIntent().getStringExtra("esta_atrapado");



        TextView tvId = findViewById(R.id.tvIdDetalle);
        TextView tvNombre = findViewById(R.id.tvNameDetalle);
        TextView tvtipo = findViewById(R.id.tvTypeDetalle);
        ImageView tvImagen = findViewById(R.id.imagePokemonDetail);
        TextView tvea = findViewById(R.id.tvEADetail);


        tvId.setText(id);
        tvNombre.setText(nombre);
        tvtipo.setText(tipo);
        Picasso.get().load(imagen).error(R.drawable.img_pokemon).into(tvImagen); //PICASSO
        tvea.setText(ea);
    }
}